cmake_minimum_required(VERSION 3.7)
project(redis_demo)
set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES main.cpp)
add_subdirectory(deps)

add_executable(redis_demo ${SOURCE_FILES})
add_dependencies(redis_demo Redis)
target_include_directories(redis_demo PUBLIC ${redis_install_dir})
